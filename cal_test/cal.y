%{
#include<stdio.h>
#include "lex_cal.cpp"
#define YYSTYLE int
int yyparse(void);
void yyerror (const char *s);
%}
%token INTEGER PLUS MINUS TIMES DIVIDE LP RP
%%
command:exp {printf("%d\1",$1);}
exp:exp PLUS term {$$ = $1 + $3;}
   | exp MINUS term {$$ = $1 - $3;}
   | term {$$ = $1;}
term:term TIMES factor {$$ = $1 * $3;}
    | term DIVIDE factor {$$ = $1 / $3;}
    | factor;
factor:INTEGER {$$ = $1;}
    | LP exp RP{$$ = $2;}
%%

int main(int argc, char ** argv) {
   return yyparse();
}

void yyerror (const char *s) {
   fprintf (stderr, "%s\n",s);
}

